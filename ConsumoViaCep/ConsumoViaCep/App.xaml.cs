﻿using ConsumoViaCep.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ConsumoViaCep
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new ConsultarCepView());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
